﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using B4.Lab4.LanceYapuchura.Models;

namespace B4.Lab4.LanceYapuchura.Tests
{
    public class GameControllerIntegrationTests
    {
        private readonly HttpClient _client;
        private readonly string _baseUrl;

        public GameControllerIntegrationTests()
        {
            _client = new HttpClient();
            _baseUrl = "http://localhost:5234";
        }

        [Test]
        public async Task CreateTwoVideoGames()
        {
            // Autenticación como administrador para obtener el token JWT
            var token = await AuthenticateAdminAsync("admin", "adminpassword");

            // Configuración de la solicitud con el token de autorización
            _client.DefaultRequestHeaders.Authorization =
                new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            // Creación de dos juegos de video
            var game1 = new Game {Name = "Game 1", Platform = "Platform 1", Price = 29.99m };
            var game2 = new Game {Name = "Game 2", Platform = "Platform 2", Price = 39.99m };

            // Serialización de los juegos a JSON
            var game1Json = JsonConvert.SerializeObject(game1);
            var game2Json = JsonConvert.SerializeObject(game2);
            
            Console.WriteLine("Contenido de game1Json:");
            Console.WriteLine(game1Json);

            Console.WriteLine("\nContenido de game2Json:");
            Console.WriteLine(game2Json);

            // Envío de las solicitudes POST para crear los juegos
            var response1 = await _client.PostAsync($"{_baseUrl}/Game",
                new StringContent(game1Json, Encoding.UTF8, "application/json"));
            var response2 = await _client.PostAsync($"{_baseUrl}/Game",
                new StringContent(game2Json, Encoding.UTF8, "application/json"));

            // Verificación de que ambas solicitudes POST fueron exitosas
            Assert.That(response1.IsSuccessStatusCode, Is.True, "Failed to create Game 1");
            Assert.That(response2.IsSuccessStatusCode, Is.True, "Failed to create Game 2");
        }

        [Test]
        public async Task VerifyTwoVideoGamesCreated()
        {
            // Obtención de la lista de juegos después de la creación
            var getResponse = await _client.GetAsync($"{_baseUrl}/Game");
            var games = JsonConvert.DeserializeObject<IEnumerable<Game>>(await getResponse.Content.ReadAsStringAsync());

            // Verificación de que se hayan creado los dos juegos
            Assert.That(games, Has.Exactly(2).Items, "Incorrect number of games created");
        }

        [Test]
        public async Task VerifyVideoGameNames()
        {
            // Obtención de la lista de juegos después de la creación
            var getResponse = await _client.GetAsync($"{_baseUrl}/Game");
            var games = JsonConvert.DeserializeObject<IEnumerable<Game>>(await getResponse.Content.ReadAsStringAsync());

            // Verificación de que los nombres de los juegos coinciden con los agregados anteriormente
            Assert.That(games,
                Has.Exactly(1).Matches<Game>(g => g.Name == "Game 1" && g.Platform == "Platform 1" && g.Price == 29.99m));
            Assert.That(games,
                Has.Exactly(1).Matches<Game>(g => g.Name == "Game 2" && g.Platform == "Platform 2" && g.Price == 39.99m));
        }

        private async Task<string> AuthenticateAdminAsync(string username, string password)
        {
            var authenticateRequest = new AuthenticateRequest { Username = username, Password = password };
            var authenticateRequestJson = JsonConvert.SerializeObject(authenticateRequest);

            var response = await _client.PostAsync($"{_baseUrl}/User/authenticate", new StringContent(authenticateRequestJson, Encoding.UTF8, "application/json"));

            if (!response.IsSuccessStatusCode)
            {
                // Si la solicitud no fue exitosa, lanzar una excepción o manejar el error de alguna otra manera
                throw new HttpRequestException($"Failed to authenticate admin. Status code: {response.StatusCode}");
            }

            var responseContent = await response.Content.ReadAsStringAsync();
            var token = JsonConvert.DeserializeObject<dynamic>(responseContent).Token;

            return token;
        }
    }
}
