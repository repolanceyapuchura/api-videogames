﻿namespace B4.Lab4.LanceYapuchura.Models;

public class AuthenticateRequest
{
    public string Username { get; set; }
    public string Password { get; set; }
}