﻿namespace B4.Lab4.LanceYapuchura.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string PasswordHash { get; set; }
        public string Role { get; set; }
        public string Token { get; set; }
    }
}