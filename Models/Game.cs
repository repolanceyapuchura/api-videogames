﻿namespace B4.Lab4.LanceYapuchura.Models
{
    public class Game
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Platform { get; set; }
        public decimal Price { get; set; }
    }
}
