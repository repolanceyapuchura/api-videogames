﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using B4.Lab4.LanceYapuchura.Models;
using B4.Lab4.LanceYapuchura.Services;
using System;

namespace B4.Lab4.LanceYapuchura.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class GameController : ControllerBase
    {
        private readonly IGameService _gameService;

        public GameController(IGameService gameService)
        {
            _gameService = gameService;
        }

        [HttpGet]
        public IEnumerable<Game> Get()
        {
            return _gameService.GetAllGames();
        }

        [HttpGet("{id}")]
        public ActionResult<Game> Get(int id)
        {
            var game = _gameService.GetGameById(id);
            if (game == null)
                return NotFound();
            return game;
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public ActionResult<Game> Post(Game game)
        {
            try
            {
                var user = HttpContext.User;
                _gameService.CreateGame(game, user);
                return CreatedAtAction(nameof(Get), new { id = game.Id }, game);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Authorize(Roles = "admin")]
        [HttpPut("{id}")]
        public IActionResult Put(int id, Game game)
        {
            try
            {
                var user = HttpContext.User;
                _gameService.UpdateGame(id, game, user);
                return NoContent();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Authorize(Roles = "admin")]
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                var user = HttpContext.User;
                _gameService.DeleteGame(id, user);
                return NoContent();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
