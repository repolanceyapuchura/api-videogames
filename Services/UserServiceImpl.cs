﻿using B4.Lab4.LanceYapuchura.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace B4.Lab4.LanceYapuchura.Services
{
    public class UserServiceImpl : IUserService
    {
        private static List<User> _users = new List<User>
        {
            new User { Id = 1, Username = "user1", PasswordHash = "password1", Role = "user" },
            new User { Id = 2, Username = "user2", PasswordHash = "password2", Role = "user" },
            new User { Id = 3, Username = "admin", PasswordHash = "adminpassword", Role = "admin" },
            new User { Id = 4, Username = "admin2", PasswordHash = "admin2", Role = "admin" }
        };

        private readonly string _secretKey;

        public UserServiceImpl(string secretKey)
        {
            _secretKey = secretKey;
        }

        public User Authenticate(string username, string password)
        {
            var user = _users.SingleOrDefault(x => x.Username == username && x.PasswordHash == password);
            if (user == null)
                return null;

            // Genera token JWT
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_secretKey);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString()),
                    new Claim(ClaimTypes.Role, user.Role)
                }),
                Expires = DateTime.UtcNow.AddHours(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key),
                    SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);

            user.PasswordHash = null;

            return user;
        }


        public User GetById(int id)
        {
            return _users.FirstOrDefault(x => x.Id == id);
        }

        public User Create(User user, string password)
        {
            // Validación
            if (string.IsNullOrWhiteSpace(password) || password.Length < 8 || !password.Any(char.IsDigit))
                throw new ArgumentException("La contraseña debe tener al menos 8 caracteres y al menos un número.");

            if (_users.Any(x => x.Username == user.Username))
                throw new InvalidOperationException("El nombre de usuario '" + user.Username + "' ya está en uso.");

            user.Id = _users.Count > 0 ? _users.Max(x => x.Id) + 1 : 1;
            user.PasswordHash =
                password; // En una implementación real, deberías almacenar el hash de la contraseña en lugar de la contraseña real
            _users.Add(user);

            return user;
        }
    }
}