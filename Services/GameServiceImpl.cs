﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using B4.Lab4.LanceYapuchura.Models;

namespace B4.Lab4.LanceYapuchura.Services
{
    public class GameServiceImpl : IGameService
    {
        private static List<Game> _games = new List<Game>();
        private static int _idCounter = 1;

        public IEnumerable<Game> GetAllGames()
        {
            return _games;
        }

        public Game GetGameById(int id)
        {
            return _games.FirstOrDefault(game => game.Id == id);
        }

        public Game CreateGame(Game game, ClaimsPrincipal user)
        {
            if (!IsAdmin(user))
                throw new UnauthorizedAccessException("Only admin users can create games.");

            if (string.IsNullOrEmpty(game.Name) || string.IsNullOrEmpty(game.Platform) || game.Price < 0)
                throw new ArgumentException("Invalid game data.");

            if (_games.Any(existingGame => existingGame.Name == game.Name))
                throw new InvalidOperationException("A game with the same name already exists.");

            game.Id = _idCounter++;
            _games.Add(game);
            return game;
        }

        public void UpdateGame(int id, Game game, ClaimsPrincipal user)
        {
            if (!IsAdmin(user))
                throw new UnauthorizedAccessException("Only admin users can update games.");
            if (string.IsNullOrEmpty(game.Name) || string.IsNullOrEmpty(game.Platform) || game.Price < 0)
                throw new ArgumentException("Invalid game data.");

            var existingGame = _games.FirstOrDefault(g => g.Id == id);
            if (existingGame == null)
                throw new KeyNotFoundException("Game not found.");

            game.Id = id;
            _games[_games.IndexOf(existingGame)] = game;
        }

        public void DeleteGame(int id, ClaimsPrincipal user)
        {
            if (!IsAdmin(user))
                throw new UnauthorizedAccessException("Only admin users can delete games.");
            var existingGame = _games.FirstOrDefault(game => game.Id == id);
            if (existingGame != null)
                _games.Remove(existingGame);
        }
        
        private bool IsAdmin(ClaimsPrincipal user)
        {
            return user.IsInRole("admin");
        }
    }
}