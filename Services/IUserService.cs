﻿using B4.Lab4.LanceYapuchura.Models;
using System.Collections.Generic;

namespace B4.Lab4.LanceYapuchura.Services
{
    public interface IUserService
    {
        User Authenticate(string username, string password);
        User GetById(int id);
        User Create(User user, string password);
    }
}