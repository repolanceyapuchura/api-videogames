﻿using B4.Lab4.LanceYapuchura.Models;
using System.Collections.Generic;
using System.Security.Claims;

namespace B4.Lab4.LanceYapuchura.Services
{
    public interface IGameService
    {
        IEnumerable<Game> GetAllGames();
        Game GetGameById(int id);
        Game CreateGame(Game game, ClaimsPrincipal user);
        void UpdateGame(int id, Game game, ClaimsPrincipal user);
        void DeleteGame(int id, ClaimsPrincipal user);
    }
}